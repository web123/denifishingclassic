<?php
/**
 * Template Name: Blog Main
 *
 */
//get_header(); ?>

                        

<div class="custom__blogs_h">
<?php
            $args = array('post_type' => 'news', 'posts_per_page' => -1, 'order' => 'DESC' );
            $mypost = new WP_Query($args);
            global $post;
            $posts = $mypost->get_posts();
foreach ($posts as $sin_post) { 
//echo "<pre>"; print_r($post);
                ?>

    <div class="i001-list-item11 cms-mg-obj">
        
<?php $image=get_the_post_thumbnail_url($sin_post->ID, 'full'); 
        if(!empty($image))
{


            ?>
        <div class="i001-list-image11" style="padding-right: 24px;">
            

           <a href="#" >
                <img src="<?php echo get_the_post_thumbnail_url($sin_post->ID, 'full') ?>" alt="">
            </a>
            

        </div>
         <?php } ?> 
        <div class="i001-list-wrap11">
        
            <h4 class="h4h">
                <a style="color: #0096d6; text-decoration: underline;" href="<?php echo get_permalink($sin_post->ID); ?>"><?php echo $sin_post->post_title ?></a>
            </h4>
            
            <h5 style="color: #0096d6;text-decoration: underline;font-weight: bold;font-size: 9pt; font-family: Arial, sans-serif;    padding-bottom: 17px;"><?php echo date('j-n-Y h:i A', strtotime($sin_post->post_date));  ?></h5>
           
      <p style="font-family: Arial, sans-serif;  font-size: 9pt;color: #4e4e4e;    padding-bottom: 17px;">
         <?php 
                            $content = $sin_post->post_content;
                            $content = preg_replace("/<img[^>]+\>/i", " ", $content);          
                            $content = apply_filters('the_content', $content);
                            $content = str_replace(']]>', ']]>', $content);
echo wp_trim_words( $content, 40, '...' ); ?> </p> 
            <!-- <p style="font-family: Arial, sans-serif;  font-size: 9pt;color: #4e4e4e;"><?php echo $trimmed = wp_trim_words( $post->post_content, $num_words = 35, $more = null )."..."; ?>  </p> -->

            <p> 
            <?php echo '<a type="button"  href="' . post_permalink($sin_post->ID) . '" class="new_v0111">Read More</a>'; ?>
            </p>
            
        
        

        </div>
        
        
    </div>
    <?php } ////ENd of foreach ?>
</div>

<?php //get_footer(); ?>
  

<style type="text/css">


</style>