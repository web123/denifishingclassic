<?php

/**
 *	Web123 WordPress Theme
 *
 */



// This will enqueue style.css of child theme



function enqueue_childtheme_scripts() {

	wp_enqueue_style( 'web123-child', get_stylesheet_directory_uri() . '/style.css', array(),4.8 );

	wp_enqueue_script( 'web123', esc_url( trailingslashit( get_stylesheet_directory_uri() ) . 'js/web123-child.min.js' ) );

}

add_action( 'wp_enqueue_scripts', 'enqueue_childtheme_scripts', 100 );







function custom_login_logo() {

    echo '<style type="text/css">

        h1 a { background-image:url(https://www.web123.com.au/wp-content/uploads/2016/09/logo-retina.png) !important; }

				.login h1 a { width: 100%!important; background-size: 160px!important; height: 200px!important;}

				body {background-color: #000!important; }

    </style>';

}



add_action('login_head', 'custom_login_logo');

function the_url( $url ) {

    return 'https://www.web123.com.au/';

}

add_filter( 'login_headerurl', 'the_url' );



 
//custome post type for News start
register_post_type( 'News',
    array(
        'labels' => array(
                'name' => __( 'News' ),
                'singular_name' => __( 'News' ),
                'add_new' => __( 'Add New' ),
                'add_new_item' => __( 'Add New News' ),
                'edit' => __( 'Edit' ),
                'edit_item' => __( 'Edit News' ),
                'new_item' => __( 'New News' ),
                'view' => __( 'View News' ),
                'view_item' => __( 'View News' ),
                'search_items' => __( 'Search News' ),
                'not_found' => __( 'No News' ),
                'not_found_in_trash' => __( 'No News found in Trash' ),
                'parent' => __( 'Parent News' ),

        ),
        'public' => true,

        'show_ui' => true,
        'hierarchical' => true,
        'query_var' => true,
        'rewrite'      => array( 'slug' => 'news', 'with_front' => false ),
        'supports' => array('title', 'editor', 'author','custom-fields','page-attributes','thumbnail','excerpt'),

    )
);


//custome post type for News End

function show_loop_blog(){







include( get_theme_file_path().'/loop_blog.php' ); 





}





add_shortcode( 'showblogs', 'show_loop_blog' );





add_filter( 'template_include', 'include_template_function', 1 );



function include_template_function( $template_path ) {

    if ( get_post_type() == 'news' ) {

        if ( is_single() ) {

            // checks if the file exists in the theme first,

            // otherwise serve the file from the plugin

            if ( $theme_file = locate_template( array ( 'single_blog.php' ) ) ) {

                $template_path = $theme_file;

            } else {

                $template_path = get_theme_file_path().'/loop_blog.php';

            }

        }

    }

    return $template_path;

}



// Unset URL from comment form

function crunchify_move_comment_form_below( $fields ) { 

    $comment_field = $fields['comment']; 

    unset( $fields['comment'] ); 

    unset( $fields['url'] ); 

    

    $fields['comment'] = $comment_field;



    return $fields; 

} 

add_filter( 'comment_form_fields', 'crunchify_move_comment_form_below' );



// Add placeholder for Name and Email

function modify_comment_form_fields($fields){

   



   $fields['subject'] = '<p class="comment-form-url">' .

   '<label for="url">' . __( 'Subject', 'domainreference' ) . '</label>' .

       '<input id="subject" name="subject" placeholder="" type="text" value="' . esc_attr( $commenter['comment_author_subject'] ) . '" size="30" /> ' .

      

             '</p>';

    return $fields;

}

add_filter('comment_form_default_fields','modify_comment_form_fields');




function hook_css() {  ?> 
<style> 
.q_logo {  

    left: 135px!important;

}
nav.main_menu>ul>li>a {
    
    padding: 0 10px !important;}
.privacy a img {
    width: 51px;
}
.four_columns>.column1, .four_columns>.column2, .four_columns>.column3, .four_columns>.column4 {
    width: 21%!important;
   
}
.four_columns.clearfix {
    position: relative;
    left: 108px;
}
input.button1111 {
    text-decoration: none;
    background: #f8f8f8;
    border: 1px solid #ebebeb;
    padding: 4px 15px 4px 15px;
    font-size: 8pt;
    color: #514742;
    border-radius: 3px;
    cursor: pointer;
    text-transform: capitalize;
 padding-right: 2px;

}
input.wpcf7-form-control.wpcf7-submit.btn.pull-right.btn-default {
font-weight: normal!important;
}

input.wpcf7-form-control.wpcf7-submit.btn.pull-right.btn-default1 {
font-weight: normal!important;
}
p.widp {
    text-align: justify;
    line-height: 20px;
    word-spacing: -1px;
}
.hist {
    padding-top: 23px;
    padding-left: 24px;
}
.image.responsive a {
    height: 680px;
    width: 100%;
    position: absolute;
}
.pull-right {
float: none!important;
}
</style> 
<?php } 
add_action('wp_head', 'hook_css');



