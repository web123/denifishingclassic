<?php
/**
 * Template Name: Blog Main
 *
 */
get_header(); ?>

                        

<div class="custom__blogs_h">
<?php
            $args = array('post_type' => 'post', 'posts_per_page' => -1, 'order' => 'ASC' );
            $mypost = new WP_Query($args);
            global $post;
            $posts = $mypost->get_posts();
            foreach ($posts as $post) { 
//echo "<pre>"; print_r($post);
            	?>

	<div class="i001-list-item cms-mg-obj">
	<div class="i001-extras0"><div class="i001-extras1"><div class="i001-extras2"><div class="i001-extras3">Comments: <span> <?php echo $my_var = get_comments_number($post->ID, 'full'); ?></span> 
</div></div></div></div>
	
		<div class="i001-list-image">
			<a href="#">
				<img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full') ?>" alt="">
			</a>
		</div>
		<div class="i001-list-wrap">
			<h4>
				<a href="<?php echo get_post_permalink($post->ID); ?>"><?php echo $post->post_title ?></a>
			</h4>
			<h5><?php echo date('j-n-Y h:i A', strtotime($post->post_date));  ?></h5>
				
				<?php echo $trimmed = wp_trim_words( $post->post_content, $num_words = 35, $more = null )."..."; ?>
			
			<p> 
			<?php echo '<a type="button"  href="' . get_post_permalink($post->ID) . '" class="i001-css-button new_v01">Read More</a>'; ?>
			</p>
		

		</div>
	</div>
	<?php } ?>
</div>

<?php get_footer(); ?>